# Luxembourg dataset

- Buildings from city of Sanem Luxembourg (`Sanem.gml`)
- OpenStreetMap data for tree positions (`arbres.gpkg`)
- OpenStreetMap data for main roads (`routes.gpkg`)
- DEM data (`terrain.tif`)

This is inspired from Lutra blog : [Working with QGIS 3D - Part 2](https://www.lutraconsulting.co.uk/blog/2020/02/28/working-with-qgis-3d-part-2/).

`gml` data are from Luxembourg OpenData: https://data.public.lu/en/datasets/5cecd25a4384b06ad27e5c58/

# Bugs

## Building edges

![](./images/bordure.png)

## Building elevations

![](./images/hauteur-batiments.png)

## Vertice rendering of buildings

![](./images/option-sommets.png)

![](./images/rendu-sommets.png)

## Underground trees

Unable to set elevation for POINT geometries.

![](./images/arbres.png)

## Flying roads

With a a terrain tile resolution set to 256 px and buildings and roads elevation based on vertex.

![](./images/flying_roads.gif)
