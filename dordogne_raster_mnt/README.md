# Dordogne dataset

This project must load a DEM from IGN.

## Issues

* unable to have the DEM loaded in the 3D view: QGIS loads tiles infinitly.

```raw
src/core/maprenderer/qgsmaprenderercustompainterjob.cpp:489 : (doRender) [4ms] [thread:0x563f089ea6a0] Rendering completed in (seconds): 0.004
src/3d/qgs3dmapscene.cpp:484 : (onFrameTriggered) [45ms] need for update
==> src/3d/chunks/qgschunkedentity_p.cpp:219 : (update) [1ms] update: active 18 enabled 7 disabled 2 | culled 7 | loading 33 loaded 1 | unloaded 1 elapsed %8ms
src/core/providers/gdal/qgsgdalprovider.cpp:3236 : (initIfNeeded) [0ms] [thread:0x563f089e9280] GdalDataset opened
src/core/providers/gdal/qgsgdalprovider.cpp:3713 : (initBaseDataset) [22ms] [thread:0x563f089e9280] GDALGetRasterNoDataValue = -9999
src/core/qgsscalecalculator.cpp:41 : (setMapUnits) [364ms] Map units set to Unknown
src/core/qgsscalecalculator.cpp:41 : (setMapUnits) [0ms] Map units set to Meters
src/core/maprenderer/qgsmaprendererjob.cpp:465 : (prepareJobs) [0ms] layer IGN_rivière_Dordogne:  minscale:1e+08  maxscale:0  scaledepvis:0  blendmode:0 isValid:1
src/core/maprenderer/qgsmaprendererjob.cpp:508 : (prepareJobs) [0ms] extent: 541281.9375000000000000,6428266.9375000000000000 : 543268.2500000000000000,6430253.2500000000000000
src/core/raster/qgsrasterlayerrenderer.cpp:228 : (QgsRasterLayerRenderer) [0ms] mapUnitsPerPixel = 3.87952
src/core/raster/qgsrasterlayerrenderer.cpp:229 : (QgsRasterLayerRenderer) [0ms] mWidth = 31781
src/core/raster/qgsrasterlayerrenderer.cpp:230 : (QgsRasterLayerRenderer) [0ms] mHeight = 26987
src/core/raster/qgsrasterlayerrenderer.cpp:231 : (QgsRasterLayerRenderer) [0ms] myRasterExtent.xMinimum() = 541282
src/core/raster/qgsrasterlayerrenderer.cpp:232 : (QgsRasterLayerRenderer) [0ms] myRasterExtent.xMaximum() = 543268
src/core/raster/qgsrasterlayerrenderer.cpp:233 : (QgsRasterLayerRenderer) [0ms] myRasterExtent.yMinimum() = 6.42827e+06
src/core/raster/qgsrasterlayerrenderer.cpp:234 : (QgsRasterLayerRenderer) [0ms] myRasterExtent.yMaximum() = 6.43025e+06
src/core/raster/qgsrasterlayerrenderer.cpp:236 : (QgsRasterLayerRenderer) [0ms] mTopLeftPoint.x() = 0
src/core/raster/qgsrasterlayerrenderer.cpp:237 : (QgsRasterLayerRenderer) [0ms] mBottomRightPoint.x() = 512
src/core/raster/qgsrasterlayerrenderer.cpp:238 : (QgsRasterLayerRenderer) [0ms] mTopLeftPoint.y() = 0
src/core/raster/qgsrasterlayerrenderer.cpp:239 : (QgsRasterLayerRenderer) [0ms] mBottomRightPoint.y() = 512
src/core/raster/qgsrasterlayerrenderer.cpp:241 : (QgsRasterLayerRenderer) [0ms] mWidth = 512
src/core/raster/qgsrasterlayerrenderer.cpp:242 : (QgsRasterLayerRenderer) [0ms] mHeight = 512
src/core/maprenderer/qgsmaprenderercustompainterjob.cpp:489 : (doRender) [34ms] [thread:0x563f089ea6a0] Rendering completed in (seconds): 0.034
src/3d/qgs3dmapscene.cpp:484 : (onFrameTriggered) [60ms] need for update
==> src/3d/chunks/qgschunkedentity_p.cpp:219 : (update) [1ms] update: active 14 enabled 2 disabled 7 | culled 8 | loading 33 loaded 1 | unloaded 1 elapsed %8ms
src/core/providers/gdal/qgsgdalprovider.cpp:3236 : (initIfNeeded) [0ms] [thread:0x563f08b515e0] GdalDataset opened
src/core/providers/gdal/qgsgdalprovider.cpp:3713 : (initBaseDataset) [26ms] [thread:0x563f08b515e0] GDALGetRasterNoDataValue = -9999
src/core/qgsscalecalculator.cpp:41 : (setMapUnits) [510ms] Map units set to Unknown
src/core/qgsscalecalculator.cpp:41 : (setMapUnits) [0ms] Map units set to Meters
src/core/maprenderer/qgsmaprendererjob.cpp:465 : (prepareJobs) [0ms] layer IGN_rivière_Dordogne:  minscale:1e+08  maxscale:0  scaledepvis:0  blendmode:0 isValid:1
src/core/maprenderer/qgsmaprendererjob.cpp:508 : (prepareJobs) [0ms] extent: 543268.2500000000000000,6414362.7500000000000000 : 551213.5000000000000000,6422308.0000000000000000
src/core/raster/qgsrasterlayerrenderer.cpp:228 : (QgsRasterLayerRenderer) [0ms] mapUnitsPerPixel = 15.5181
src/core/raster/qgsrasterlayerrenderer.cpp:229 : (QgsRasterLayerRenderer) [0ms] mWidth = 31781
src/core/raster/qgsrasterlayerrenderer.cpp:230 : (QgsRasterLayerRenderer) [0ms] mHeight = 26987
src/core/raster/qgsrasterlayerrenderer.cpp:231 : (QgsRasterLayerRenderer) [0ms] myRasterExtent.xMinimum() = 543268
src/core/raster/qgsrasterlayerrenderer.cpp:232 : (QgsRasterLayerRenderer) [0ms] myRasterExtent.xMaximum() = 551214
src/core/raster/qgsrasterlayerrenderer.cpp:233 : (QgsRasterLayerRenderer) [0ms] myRasterExtent.yMinimum() = 6.41436e+06
src/core/raster/qgsrasterlayerrenderer.cpp:234 : (QgsRasterLayerRenderer) [0ms] myRasterExtent.yMaximum() = 6.42231e+06
src/core/raster/qgsrasterlayerrenderer.cpp:236 : (QgsRasterLayerRenderer) [0ms] mTopLeftPoint.x() = 0
src/core/raster/qgsrasterlayerrenderer.cpp:237 : (QgsRasterLayerRenderer) [0ms] mBottomRightPoint.x() = 512
src/core/raster/qgsrasterlayerrenderer.cpp:238 : (QgsRasterLayerRenderer) [0ms] mTopLeftPoint.y() = 0
src/core/raster/qgsrasterlayerrenderer.cpp:239 : (QgsRasterLayerRenderer) [0ms] mBottomRightPoint.y() = 512
src/core/raster/qgsrasterlayerrenderer.cpp:241 : (QgsRasterLayerRenderer) [0ms] mWidth = 512
src/core/raster/qgsrasterlayerrenderer.cpp:242 : (QgsRasterLayerRenderer) [0ms] mHeight = 512
src/core/maprenderer/qgsmaprenderercustompainterjob.cpp:489 : (doRender) [27ms] [thread:0x563f08c0cad0] Rendering completed in (seconds): 0.027
src/3d/qgs3dmapscene.cpp:484 : (onFrameTriggered) [57ms] need for update
==> src/3d/chunks/qgschunkedentity_p.cpp:219 : (update) [1ms] update: active 12 enabled 6 disabled 9 | culled 4 | loading 33 loaded 1 | unloaded 1 elapsed %8ms
src/core/providers/gdal/qgsgdalprovider.cpp:3236 : (initIfNeeded) [0ms] [thread:0x563f08b51700] GdalDataset opened
src/core/providers/gdal/qgsgdalprovider.cpp:3713 : (initBaseDataset) [19ms] [thread:0x563f08b51700] GDALGetRasterNoDataValue = -9999
src/core/qgsscalecalculator.cpp:41 : (setMapUnits) [114ms] Map units set to Unknown
src/core/qgsscalecalculator.cpp:41 : (setMapUnits) [0ms] Map units set to Meters
src/core/maprenderer/qgsmaprendererjob.cpp:465 : (prepareJobs) [0ms] layer IGN_rivière_Dordogne:  minscale:1e+08  maxscale:0  scaledepvis:0  blendmode:0 isValid:1
src/core/maprenderer/qgsmaprendererjob.cpp:508 : (prepareJobs) [0ms] extent: 543268.2500000000000000,6430253.2500000000000000 : 551213.5000000000000000,6438198.5000000000000000
src/core/raster/qgsrasterlayerrenderer.cpp:228 : (QgsRasterLayerRenderer) [0ms] mapUnitsPerPixel = 15.5181
src/core/raster/qgsrasterlayerrenderer.cpp:229 : (QgsRasterLayerRenderer) [0ms] mWidth = 31781
src/core/raster/qgsrasterlayerrenderer.cpp:230 : (QgsRasterLayerRenderer) [0ms] mHeight = 26987
src/core/raster/qgsrasterlayerrenderer.cpp:231 : (QgsRasterLayerRenderer) [0ms] myRasterExtent.xMinimum() = 543268
src/core/raster/qgsrasterlayerrenderer.cpp:232 : (QgsRasterLayerRenderer) [0ms] myRasterExtent.xMaximum() = 551214
src/core/raster/qgsrasterlayerrenderer.cpp:233 : (QgsRasterLayerRenderer) [0ms] myRasterExtent.yMinimum() = 6.43025e+06
src/core/raster/qgsrasterlayerrenderer.cpp:234 : (QgsRasterLayerRenderer) [0ms] myRasterExtent.yMaximum() = 6.43342e+06
src/core/raster/qgsrasterlayerrenderer.cpp:236 : (QgsRasterLayerRenderer) [0ms] mTopLeftPoint.x() = 0
src/core/raster/qgsrasterlayerrenderer.cpp:237 : (QgsRasterLayerRenderer) [0ms] mBottomRightPoint.x() = 512
src/core/raster/qgsrasterlayerrenderer.cpp:238 : (QgsRasterLayerRenderer) [0ms] mTopLeftPoint.y() = 308
src/core/raster/qgsrasterlayerrenderer.cpp:239 : (QgsRasterLayerRenderer) [0ms] mBottomRightPoint.y() = 512
src/core/raster/qgsrasterlayerrenderer.cpp:241 : (QgsRasterLayerRenderer) [0ms] mWidth = 512
src/core/raster/qgsrasterlayerrenderer.cpp:242 : (QgsRasterLayerRenderer) [0ms] mHeight = 204
src/core/maprenderer/qgsmaprenderercustompainterjob.cpp:489 : (doRender) [5ms] [thread:0x563f089ea6a0] Rendering completed in (seconds): 0.004
src/3d/qgs3dmapscene.cpp:484 : (onFrameTriggered) [52ms] need for update
==> src/3d/chunks/qgschunkedentity_p.cpp:219 : (update) [1ms] update: active 18 enabled 7 disabled 2 | culled 7 | loading 33 loaded 1 | unloaded 1 elapsed %8ms
src/core/providers/gdal/qgsgdalprovider.cpp:3236 : (initIfNeeded) [0ms] [thread:0x563f089e92b0] GdalDataset opened
src/core/providers/gdal/qgsgdalprovider.cpp:3713 : (initBaseDataset) [26ms] [thread:0x563f089e92b0] GDALGetRasterNoDataValue = -9999
src/core/qgsscalecalculator.cpp:41 : (setMapUnits) [383ms] Map units set to Unknown
src/core/qgsscalecalculator.cpp:41 : (setMapUnits) [0ms] Map units set to Meters
src/core/maprenderer/qgsmaprendererjob.cpp:465 : (prepareJobs) [0ms] layer IGN_rivière_Dordogne:  minscale:1e+08  maxscale:0  scaledepvis:0  blendmode:0 isValid:1
src/core/maprenderer/qgsmaprendererjob.cpp:508 : (prepareJobs) [0ms] extent: 539295.6250000000000000,6428266.9375000000000000 : 541281.9375000000000000,6430253.2500000000000000
src/core/raster/qgsrasterlayerrenderer.cpp:228 : (QgsRasterLayerRenderer) [0ms] mapUnitsPerPixel = 3.87952
src/core/raster/qgsrasterlayerrenderer.cpp:229 : (QgsRasterLayerRenderer) [0ms] mWidth = 31781
src/core/raster/qgsrasterlayerrenderer.cpp:230 : (QgsRasterLayerRenderer) [0ms] mHeight = 26987
src/core/raster/qgsrasterlayerrenderer.cpp:231 : (QgsRasterLayerRenderer) [0ms] myRasterExtent.xMinimum() = 539296
src/core/raster/qgsrasterlayerrenderer.cpp:232 : (QgsRasterLayerRenderer) [0ms] myRasterExtent.xMaximum() = 541282
src/core/raster/qgsrasterlayerrenderer.cpp:233 : (QgsRasterLayerRenderer) [0ms] myRasterExtent.yMinimum() = 6.42827e+06
src/core/raster/qgsrasterlayerrenderer.cpp:234 : (QgsRasterLayerRenderer) [0ms] myRasterExtent.yMaximum() = 6.43025e+06
src/core/raster/qgsrasterlayerrenderer.cpp:236 : (QgsRasterLayerRenderer) [0ms] mTopLeftPoint.x() = 0
src/core/raster/qgsrasterlayerrenderer.cpp:237 : (QgsRasterLayerRenderer) [0ms] mBottomRightPoint.x() = 512
src/core/raster/qgsrasterlayerrenderer.cpp:238 : (QgsRasterLayerRenderer) [0ms] mTopLeftPoint.y() = 0
src/core/raster/qgsrasterlayerrenderer.cpp:239 : (QgsRasterLayerRenderer) [0ms] mBottomRightPoint.y() = 512
src/core/raster/qgsrasterlayerrenderer.cpp:241 : (QgsRasterLayerRenderer) [0ms] mWidth = 512
src/core/raster/qgsrasterlayerrenderer.cpp:242 : (QgsRasterLayerRenderer) [0ms] mHeight = 512
src/core/maprenderer/qgsmaprenderercustompainterjob.cpp:489 : (doRender) [27ms] [thread:0x563f089e9280] Rendering completed in (seconds): 0.027
src/3d/qgs3dmapscene.cpp:484 : (onFrameTriggered) [66ms] need for update
==> src/3d/chunks/qgschunkedentity_p.cpp:219 : (update) [1ms] update: active 14 enabled 2 disabled 7 | culled 8 | loading 33 loaded 1 | unloaded 1 elapsed %8ms
src/core/providers/gdal/qgsgdalprovider.cpp:3236 : (initIfNeeded) [0ms] [thread:0x563f08abbe80] GdalDataset opened
src/core/providers/gdal/qgsgdalprovider.cpp:3713 : (initBaseDataset) [21ms] [thread:0x563f08abbe80] GDALGetRasterNoDataValue = -9999
src/core/qgsscalecalculator.cpp:41 : (setMapUnits) [325ms] Map units set to Unknown
src/core/qgsscalecalculator.cpp:41 : (setMapUnits) [0ms] Map units set to Meters
src/core/maprenderer/qgsmaprendererjob.cpp:465 : (prepareJobs) [0ms] layer IGN_rivière_Dordogne:  minscale:1e+08  maxscale:0  scaledepvis:0  blendmode:0 isValid:1
src/core/maprenderer/qgsmaprendererjob.cpp:508 : (prepareJobs) [0ms] extent: 543268.2500000000000000,6406417.5000000000000000 : 551213.5000000000000000,6414362.7500000000000000
src/core/raster/qgsrasterlayerrenderer.cpp:228 : (QgsRasterLayerRenderer) [0ms] mapUnitsPerPixel = 15.5181
src/core/raster/qgsrasterlayerrenderer.cpp:229 : (QgsRasterLayerRenderer) [0ms] mWidth = 31781
src/core/raster/qgsrasterlayerrenderer.cpp:230 : (QgsRasterLayerRenderer) [0ms] mHeight = 26987
src/core/raster/qgsrasterlayerrenderer.cpp:231 : (QgsRasterLayerRenderer) [0ms] myRasterExtent.xMinimum() = 543268
src/core/raster/qgsrasterlayerrenderer.cpp:232 : (QgsRasterLayerRenderer) [0ms] myRasterExtent.xMaximum() = 551214
src/core/raster/qgsrasterlayerrenderer.cpp:233 : (QgsRasterLayerRenderer) [0ms] myRasterExtent.yMinimum() = 6.40642e+06
src/core/raster/qgsrasterlayerrenderer.cpp:234 : (QgsRasterLayerRenderer) [0ms] myRasterExtent.yMaximum() = 6.41436e+06
src/core/raster/qgsrasterlayerrenderer.cpp:236 : (QgsRasterLayerRenderer) [0ms] mTopLeftPoint.x() = 0
src/core/raster/qgsrasterlayerrenderer.cpp:237 : (QgsRasterLayerRenderer) [0ms] mBottomRightPoint.x() = 512
src/core/raster/qgsrasterlayerrenderer.cpp:238 : (QgsRasterLayerRenderer) [0ms] mTopLeftPoint.y() = 0
src/core/raster/qgsrasterlayerrenderer.cpp:239 : (QgsRasterLayerRenderer) [0ms] mBottomRightPoint.y() = 512
src/core/raster/qgsrasterlayerrenderer.cpp:241 : (QgsRasterLayerRenderer) [0ms] mWidth = 512
src/core/raster/qgsrasterlayerrenderer.cpp:242 : (QgsRasterLayerRenderer) [0ms] mHeight = 512
src/core/maprenderer/qgsmaprenderercustompainterjob.cpp:489 : (doRender) [15ms] [thread:0x563f08aad0d0] Rendering completed in (seconds): 0.015
src/3d/qgs3dmapscene.cpp:484 : (onFrameTriggered) [51ms] need for update
==> src/3d/chunks/qgschunkedentity_p.cpp:219 : (update) [1ms] update: active 12 enabled 6 disabled 9 | culled 4 | loading 33 loaded 1 | unloaded 1 elapsed %8ms
src/core/providers/gdal/qgsgdalprovider.cpp:3236 : (initIfNeeded) [0ms] [thread:0x563f08c0cad0] GdalDataset opened
src/core/providers/gdal/qgsgdalprovider.cpp:3713 : (initBaseDataset) [20ms] [thread:0x563f08c0cad0] GDALGetRasterNoDataValue = -9999
src/core/qgsscalecalculator.cpp:41 : (setMapUnits) [113ms] Map units set to Unknown
src/core/qgsscalecalculator.cpp:41 : (setMapUnits) [0ms] Map units set to Meters
src/core/maprenderer/qgsmaprendererjob.cpp:465 : (prepareJobs) [0ms] layer IGN_rivière_Dordogne:  minscale:1e+08  maxscale:0  scaledepvis:0  blendmode:0 isValid:1
src/core/maprenderer/qgsmaprendererjob.cpp:508 : (prepareJobs) [0ms] extent: 535323.0000000000000000,6430253.2500000000000000 : 543268.2500000000000000,6438198.5000000000000000
src/core/raster/qgsrasterlayerrenderer.cpp:228 : (QgsRasterLayerRenderer) [0ms] mapUnitsPerPixel = 15.5181
src/core/raster/qgsrasterlayerrenderer.cpp:229 : (QgsRasterLayerRenderer) [0ms] mWidth = 31781
src/core/raster/qgsrasterlayerrenderer.cpp:230 : (QgsRasterLayerRenderer) [0ms] mHeight = 26987
src/core/raster/qgsrasterlayerrenderer.cpp:231 : (QgsRasterLayerRenderer) [0ms] myRasterExtent.xMinimum() = 535323
src/core/raster/qgsrasterlayerrenderer.cpp:232 : (QgsRasterLayerRenderer) [0ms] myRasterExtent.xMaximum() = 543268
src/core/raster/qgsrasterlayerrenderer.cpp:233 : (QgsRasterLayerRenderer) [0ms] myRasterExtent.yMinimum() = 6.43025e+06
src/core/raster/qgsrasterlayerrenderer.cpp:234 : (QgsRasterLayerRenderer) [0ms] myRasterExtent.yMaximum() = 6.43342e+06
src/core/raster/qgsrasterlayerrenderer.cpp:236 : (QgsRasterLayerRenderer) [0ms] mTopLeftPoint.x() = 0
src/core/raster/qgsrasterlayerrenderer.cpp:237 : (QgsRasterLayerRenderer) [0ms] mBottomRightPoint.x() = 512
src/core/raster/qgsrasterlayerrenderer.cpp:238 : (QgsRasterLayerRenderer) [0ms] mTopLeftPoint.y() = 308
src/core/raster/qgsrasterlayerrenderer.cpp:239 : (QgsRasterLayerRenderer) [0ms] mBottomRightPoint.y() = 512
src/core/raster/qgsrasterlayerrenderer.cpp:241 : (QgsRasterLayerRenderer) [0ms] mWidth = 512
src/core/raster/qgsrasterlayerrenderer.cpp:242 : (QgsRasterLayerRenderer) [0ms] mHeight = 204
src/core/maprenderer/qgsmaprenderercustompainterjob.cpp:489 : (doRender) [5ms] [thread:0x563f08b515e0] Rendering completed in (seconds): 0.004
src/3d/qgs3dmapscene.cpp:484 : (onFrameTriggered) [44ms] need for update
==> src/3d/chunks/qgschunkedentity_p.cpp:219 : (update) [1ms] update: active 18 enabled 7 disabled 2 | culled 7 | loading 33 loaded 1 | unloaded 1 elapsed %8ms
src/core/providers/gdal/qgsgdalprovider.cpp:3236 : (initIfNeeded) [0ms] [thread:0x563f089e9690] GdalDataset opened
src/core/providers/gdal/qgsgdalprovider.cpp:3713 : (initBaseDataset) [20ms] [thread:0x563f089e9690] GDALGetRasterNoDataValue = -9999
src/core/qgsscalecalculator.cpp:41 : (setMapUnits) [357ms] Map units set to Unknown
src/core/qgsscalecalculator.cpp:41 : (setMapUnits) [0ms] Map units set to Meters
src/core/maprenderer/qgsmaprendererjob.cpp:465 : (prepareJobs) [0ms] layer IGN_rivière_Dordogne:  minscale:1e+08  maxscale:0  scaledepvis:0  blendmode:0 isValid:1
src/core/maprenderer/qgsmaprendererjob.cpp:508 : (prepareJobs) [0ms] extent: 541281.9375000000000000,6428266.9375000000000000 : 543268.2500000000000000,6430253.2500000000000000
src/core/raster/qgsrasterlayerrenderer.cpp:228 : (QgsRasterLayerRenderer) [0ms] mapUnitsPerPixel = 3.87952
src/core/raster/qgsrasterlayerrenderer.cpp:229 : (QgsRasterLayerRenderer) [0ms] mWidth = 31781
src/core/raster/qgsrasterlayerrenderer.cpp:230 : (QgsRasterLayerRenderer) [0ms] mHeight = 26987
src/core/raster/qgsrasterlayerrenderer.cpp:231 : (QgsRasterLayerRenderer) [0ms] myRasterExtent.xMinimum() = 541282
src/core/raster/qgsrasterlayerrenderer.cpp:232 : (QgsRasterLayerRenderer) [0ms] myRasterExtent.xMaximum() = 543268
src/core/raster/qgsrasterlayerrenderer.cpp:233 : (QgsRasterLayerRenderer) [0ms] myRasterExtent.yMinimum() = 6.42827e+06
src/core/raster/qgsrasterlayerrenderer.cpp:234 : (QgsRasterLayerRenderer) [0ms] myRasterExtent.yMaximum() = 6.43025e+06
src/core/raster/qgsrasterlayerrenderer.cpp:236 : (QgsRasterLayerRenderer) [0ms] mTopLeftPoint.x() = 0
src/core/raster/qgsrasterlayerrenderer.cpp:237 : (QgsRasterLayerRenderer) [0ms] mBottomRightPoint.x() = 512
src/core/raster/qgsrasterlayerrenderer.cpp:238 : (QgsRasterLayerRenderer) [0ms] mTopLeftPoint.y() = 0
src/core/raster/qgsrasterlayerrenderer.cpp:239 : (QgsRasterLayerRenderer) [0ms] mBottomRightPoint.y() = 512
src/core/raster/qgsrasterlayerrenderer.cpp:241 : (QgsRasterLayerRenderer) [0ms] mWidth = 512
src/core/raster/qgsrasterlayerrenderer.cpp:242 : (QgsRasterLayerRenderer) [0ms] mHeight = 512
src/core/maprenderer/qgsmaprenderercustompainterjob.cpp:489 : (doRender) [32ms] [thread:0x563f08aad0d0] Rendering completed in (seconds): 0.032
src/3d/qgs3dmapscene.cpp:484 : (onFrameTriggered) [64ms] need for update
```

* if we activate/deactivate layers, QGIS crash
