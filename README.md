# Basic use cases for QGIS 3D


## Big file handling

[`git lfs`](https://docs.gitlab.com/ee/topics/git/lfs/) handles these file formats:

- `asc`
- `ers`
- `gml`
- `gpkg`
- `laz`
- `ply`
- `sgy`
- `stl`
- `tif`

```sh
git clone git@gitlab.com:Oslandia/qgis-3d-test-projects.git
git lfs fetch origin main
```

## Potential data sources

- [awesome-citygml](https://github.com/OloOcki/awesome-citygml)
