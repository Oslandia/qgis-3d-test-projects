# Mansfield dataset

- Digital Elevation Model and the survey data come from the Geological Survey of Victoria: https://gsv.vic.gov.au/sd_weave/anonymous.html?sessionkey=18A46AC5619-148169 ;
- The 3D grid and the surfaces have been generated from the geological data ;
- The geophysical data have been produced by combining the geological data with fictitious petrophysical parameters.

These data are freely available for non-commercial use.

## 2D view

![](./images/mansfield_3dview.png)

## 3D view

![](./images/mansfield_3dview.png)


